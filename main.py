from aws_user_credentials import user_credentials as user  # import access key and secret ascces key
from dbConfig import db
from fastapi import FastAPI
import json
from pydantic import BaseModel
import threading
import boto3
import pandas
import os
import time
from datetime import datetime

acc_key = user["password"]
sec_acc_key = user["secretKey"]

# low level functional client for single s3 file
client = boto3.client(
    's3',
    aws_access_key_id = acc_key,
    aws_secret_access_key = sec_acc_key,
    region_name = 'eu-west-3'
)


# high level object oriented interface for multiple s3 buckets
resource = boto3.resource(
    's3',
    aws_access_key_id = acc_key,
    aws_secret_access_key = sec_acc_key,
    region_name = 'eu-west-3'
)


# fetching the list of buckets
clientResponse = client.list_buckets()



# Creating a bucket in AWS S3
"""
location = {'LocationConstraint': 'eu-west-3'}
client.create_bucket(
    Bucket='awsbucket-873',
    CreateBucketConfiguration=location
)
"""

"""
# print the bucket names
print('Buckets...')
for buck in resourceResponse["Buckets"]:
    print(f'Bucket Name: {buck["Name"]}')
"""


class Employee(BaseModel):
    employee_id : str
    dept : str
    image : str
    name : str
    position : str
    regDate : str
    status : str

db_em_all = []
db_em = {}
img_url = ""

app = FastAPI()

etc = {
    "appname": "firstapp",
    "name": "abdullah"
}


@app.get("/")
def index():
    return {"appname": "firstapp"}

@app.get("/employees")
def get_employees():
    db_em_all = []
    users_ref = db.collection(u'employees').stream()

    for doc in users_ref:
        db_em_all.append(doc.to_dict())

    return db_em_all


@app.post("/employees")
def create_employees(employee: Employee):
    db_em = employee.dict()
    timeStamp = time.time()
    tarih = datetime.now().strftime('%d.%m.%Y %H:%M:%S')
    
    for file in os.listdir():
        if ".jpg" in file:
            upload_file_bucket = "awsbucket-873"
            upload_file_key = "asd.jpg"
            client.upload_file(file, upload_file_bucket, upload_file_key)
    
    db_em["image"] = "https://awsbucket-873.s3.eu-west-3.amazonaws.com/" + upload_file_key
    db_em["regDate"] = tarih
    db_em["regTimeStamp"] = timeStamp    
    db.collection(u'employees').document().set(db_em)
    return db_em

